import traceback

import logging
import os
import time
import subprocess
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from env import *

def write_log(txt):
    log_file = './logs.txt'
    logging.basicConfig(filename=log_file, filemode='a', level=logging.DEBUG)

    if type(txt) is Exception:
        logging.exception(txt)
        traceback.print_exc(txt)
    else:
        logging.debug(txt)
        traceback.print_exc(txt)
        print(txt)

def browser_wait(browser, timeout, selector):
    element_present = EC.presence_of_element_located((By.CSS_SELECTOR, selector))
    WebDriverWait(browser, timeout).until(element_present)

def get_usr_cookie_file(uid):
    return DEFAULT_DATA_USR_DIR + uid +'\\'+ DEFAULT_DATA_USR_COOKIE_FILE

def get_fanpage_list_posts_file(id_page):
    return DEFAULT_DATA_FANPAGE_DIR + id_page +'\\'+ DEFAULT_DATA_FANPAGE_LIST_POSTS_FILE

def get_group_list_posts_file(id_group):
    return DEFAULT_DATA_GROUP_DIR + id_group +'\\'+ DEFAULT_DATA_GROUP_LIST_POSTS_FILE

def get_scripts_js_file(path):
    return DEFAULT_SCRIPTS_JS_DIR + path

def write_file(path, content):
    if not os.path.exists(os.path.dirname(path)):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as exc:  # Guard against race condition
            if exc.errno != path.EEXIST:
                write_log(exc)
    with open(path, "w") as f:
        f.write(content)

def reset_dcom():
    CREATE_NO_WINDOW = 0x08000000
    subprocess.Popen(['%Windir%\\system32\\rasdial', '/disconnect'], shell=True, creationflags=CREATE_NO_WINDOW)
    write_log("Disconnected dcom 3g. Sleeping ...")
    time.sleep(10)

    subprocess.Popen(['%Windir%\\system32\\rasdial', 'viettel'], shell=True, creationflags=CREATE_NO_WINDOW)
    write_log("Connected dcom 3g. Running ...")
