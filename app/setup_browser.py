from selenium import webdriver
from utils import write_log
import datetime

def get_headers():
    # Web
    res = 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'

    # Iphone
    res = 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_0 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobile/8A293 Safari/6531.22.7'
    return res

def get_cookie_jar(cookie_file, domain):
    try:
        res = []
        with open(cookie_file, 'r') as f_cookie:
            ck_value = f_cookie.read()
            
            ck_arr = ck_value.split(';')
            for ck in ck_arr:
                s = str(ck).strip().split('=')
                if len(s) == 2:
                    name = s[0].strip()
                    value = s[1].strip()

                    # custom for google cookie
                    if name == '1P_JAR':
                        value = datetime.date.today().strftime("%Y-%m-%d-8")
                    cookie = dict(version=0, name=name, value=value, port=None, port_specified=False, domain=domain, domain_specified=False, domain_initial_dot=False, path='/', path_specified=True, secure=False, expires=None, discard=True, comment=None, comment_url=None, rest={'HttpOnly': None}, rfc2109=False)
                    res.append(cookie)
        return res
    except Exception as ex:
        write_log(ex)

def chrome_browser(is_headless, cookie_file):
    # Custom headless option
    options = webdriver.ChromeOptions()
    if is_headless:
        options.add_argument('headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--no-default-browser-check')
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-extensions')
    options.add_argument('--disable-default-apps')

    desired_capabilities = webdriver.DesiredCapabilities.CHROME.copy()
    desired_capabilities['chrome.page.customHeaders.User-Agent'] = get_headers()

    browser = webdriver.Chrome(
        executable_path='./libs/webdriver/chromedriver.exe',
        chrome_options=options,
        desired_capabilities=desired_capabilities)
        
    # Set-up cookie
    browser.get('http://facebook.com/favicon.ico')
    cookie = get_cookie_jar(cookie_file, '.facebook.com')
    for ck in cookie:
        browser.add_cookie(ck)
    return browser
