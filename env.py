# noinspection Annotator
DEFAULT_ROOT = 'D:\WorkSpace\PycharmProjects\\fb'

DEFAULT_LIBS_DIR = '{0}\libs\\'.format(DEFAULT_ROOT)
DEFAULT_LIBS_WEBDRIVER_DIR = '{0}webdriver\\'.format(DEFAULT_LIBS_DIR)

DEFAULT_LOG_DIR = '{0}\logs\\'.format(DEFAULT_ROOT)
DEFAULT_LOG_FILE = DEFAULT_LOG_DIR + 'logs.txt'

DEFAULT_SCRIPTS_DIR = '{0}\scripts\\'.format(DEFAULT_ROOT)
DEFAULT_SCRIPTS_JS_DIR = '{0}js\\'.format(DEFAULT_SCRIPTS_DIR)

DEFAULT_DATA_DIR = '{0}\data\\'.format(DEFAULT_ROOT)

DEFAULT_DATA_USR_DIR = '{0}usr\\'.format(DEFAULT_DATA_DIR)
DEFAULT_DATA_USR_COOKIE_FILE = 'cookie.txt'

DEFAULT_DATA_FANPAGE_DIR = '{0}fanpage\\'.format(DEFAULT_DATA_DIR)
DEFAULT_DATA_FANPAGE_LIST_POSTS_FILE = 'list_posts.txt'

DEFAULT_DATA_GROUP_DIR = '{0}group\\'.format(DEFAULT_DATA_DIR)
DEFAULT_DATA_GROUP_LIST_POSTS_FILE = 'list_posts.txt'

DCOM_CONNECT_PATH = '{0}\scripts\dcom\connect.bat'.format(DEFAULT_ROOT)
DCOM_DISCONNECT_PATH = '{0}\scripts\dcom\disconnect.bat'.format(DEFAULT_ROOT)
