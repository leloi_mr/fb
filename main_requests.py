# coding=utf-8
import json
import time
import cookielib
import requests
import logging
import traceback
import mechanize
import re
from bs4 import BeautifulSoup

def get_proxy():
    return False
    return {'https': '125.212.207.121:3128'}

def write_log(txt):
    global log_file
    logging.basicConfig(filename=log_file, filemode='a', level=logging.DEBUG)

    if type(txt) is Exception:
        traceback.print_exc(txt)
        logging.exception(txt)
    else:
        logging.debug(txt)

def get_cookie_jar():
    global cookie_file
    try:
        with open(cookie_file, 'r') as f_cookie:
            ck_value = f_cookie.read()
            ck = cookielib.Cookie(version=0, name=cookie_file, value=ck_value, port=None, port_specified=False, domain='www.facebook.com', domain_specified=False, domain_initial_dot=False, path='/', path_specified=True, secure=False, expires=None, discard=True, comment=None, comment_url=None, rest={'HttpOnly': None}, rfc2109=False)

            cookie_jar = cookielib.LWPCookieJar()
            cookie_jar.set_cookie(ck)
            return cookie_jar
    except Exception as ex:
        write_log(ex)
        return False

def get_headers():
    return [
        ('User-agent', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36')]

def setting_browser():
    global browser
    
    # To enable proxy
    proxy = get_proxy()
    if proxy:
        browser.set_proxies(proxy)
        
    # To enable cookies
    cookie_jar = get_cookie_jar()
    if cookie_jar:
        browser.set_cookiejar(cookie_jar)
    
    # Browser settings
    browser.set_handle_equiv(True)
    browser.set_handle_gzip(True)
    browser.set_handle_redirect(True)
    browser.set_handle_referer(True)
    browser.set_handle_robots(False)
    browser.set_handle_refresh(
        mechanize._http.HTTPRefreshProcessor(), max_time=1)

    # To set user-agent browser
    headers = get_headers()
    if headers:
        browser.addheaders = headers

def get_uid():
    ck_jar = requests.utils.dict_from_cookiejar(get_cookie_jar())
    ck_value = ck_jar[cookie_file]
    uid = re.search(r'c_user=([0-9]+);', ck_value).group(1)
    return uid

def get_dtsg(content):
    dtsg = re.search(r'(type="hidden" name="fb_dtsg" value="([0-9a-zA-Z-_:]+)")', content).group(1)
    dtsg = dtsg[dtsg.find("value")+6:]
    dtsg = dtsg[1:-1]
    return dtsg

def login_fb():
    global browser

    setting_browser()
    url = 'http://facebook.com'
    res = browser.open(url)    
    content = res.read()

    if "logoutMenu" in content:
        uid = get_uid()
        dtsg = get_dtsg(content)
        return uid, dtsg
    else:
        return False, False

def init():
    try:
        print('+ Checking login...')
        
        global uid, dtsg
        uid, dtsg = login_fb()
        if uid:
            print("+ Login Success.")

            # Run important tasks
            run()
        else:
            print("+ Login Error. Stop.")
    except Exception as ex:
        write_log(ex)

def run():
    global browser, uid, dtsg
    
    try:
        setting_browser()
        
        id_page = '526028914504611'
        url = 'https://m.facebook.com/a/group/post/add/?gid='+ id_page +'&refid=18'
        response = browser.open(url)

        message = 'testing message'

##document.querySelector('textarea.navigationFocus').value = 'abc';
##document.querySelector('textarea.navigationFocus').click();
##setTimeout(function() {
##	document.querySelector('#pagelet_group_composer button.selected').click();
##}, 2000);
        
        data = urllib.urlencode(params)
        request = mechanize.Request(url)
        response = mechanize.urlopen(request, data=data)

        txt = response.read()
        open('index.html', 'w').write(txt)
    except Exception as e:
        write_log(ex)

# Setting some value
browser = mechanize.Browser()
cookie_file = 'lanlan_cookie.txt'
log_file = 'logs.txt'
uid, dtsg = False, False

# Run the app
init()
