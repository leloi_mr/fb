background.receive("options:storage", function (e) {
  var select = document.querySelector("#method");
  if (e.webrtc) select.value = e.webrtc;
  /*  */
  var input = document.querySelector("#inject");
  if (e.inject) input.checked = e.inject;
});

var load = function () {
  background.send("options:load");
  var input = document.querySelector("#inject");
  var select = document.querySelector("#method");
  window.removeEventListener("load", load, false);
  /*  */
  select.addEventListener("change", function (e) {background.send("options:webrtc", {"webrtc": e.target.value})});
  input.addEventListener("change", function (e) {background.send("options:inject", {"inject": e.target.checked})});
};

window.addEventListener("load", load, false);
