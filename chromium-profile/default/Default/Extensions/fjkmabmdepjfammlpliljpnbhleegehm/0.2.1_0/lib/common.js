window.setTimeout(function () {
  var version = config.welcome.version;
  if (!version) {
    app.tab.open(app.homepage() + '?v=' + app.version() + "&type=install");
    config.welcome.version = app.version();
  }
}, 3000);

var update = function () {
  var state = config.addon.state;
  var iconpath = '../../data/icons/' + (state ? state + '/' : '');
  app.button.icon = {
    "path": {
      '16': iconpath + '16.png',
      '32': iconpath + '32.png',
      '48': iconpath + '48.png',
      '64': iconpath + '64.png'
    }
  };
  /*  */
  app.webRTC();
  app.button.label = "WebRTC leak protection is " + (state === "enabled" ? "ON" : "OFF");
};

app.button.onCommand(function () {
  var state = config.addon.state;
  config.addon.state = state === "disabled" ? "enabled" : "disabled";
  config.addon.state = config.addon.webrtc === "default" ? "disabled" : config.addon.state;
  update();
});

app.addon.receive("options:inject", function (e) {
  config.addon.inject = e.inject;
  update();
});

app.addon.receive("page:load", function (e) {
  app.addon.send("page:storage", {
    "state": config.addon.state,
    "inject": config.addon.inject
  }, e ? e.tabId : '');
});

app.addon.receive("options:load", function () {
  app.addon.send("options:storage", {
    "webrtc": config.addon.webrtc,
    "inject": config.addon.inject
  });
});

app.addon.receive("options:webrtc", function (e) {
  config.addon.webrtc = e.webrtc;
  config.addon.state = config.addon.webrtc === "default" ? "disabled" : "enabled";
  update();
});

window.setTimeout(update, 0);
