# coding=utf-8
from app import *
import os

# Setting
id_page_test = '1606732479581658'
# id_group_test = '1126311744160707'

def init():
    uids = ('100012767923805', '100012808450552')
    uids = ()
    is_headless = False
    i = 0
    for uid in uids:
        cookie_file = get_usr_cookie_file(uid)
        browser = chrome_browser(is_headless, cookie_file)
        url = 'https://m.facebook.com/story.php?story_fbid=1637408496514056&id=1606732479581658&fs=0&focus_composer=0'
        process_comment_to_a_post_fanpage(browser, url, 10)

        # Do some tasks
        #post_to_page(id_page_test, 10)
        # get_list_post_from_page(browser, id_page_test, 10)
        # process_list_post_from_page(browser, id_page_test, 10)
        #post_to_group(browser, id_group_test, 10)
        #get_list_post_from_group(browser, id_group_test, 10)
        # process_list_post_from_group(browser, id_group_test, 10)
        browser.quit()

        # wait for a time (second) before next
        i += 1
        if i == 5:
            reset_dcom()
        else:
            time.sleep(5)
    exit(0)

# Run
init()
